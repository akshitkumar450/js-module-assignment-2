class Person {
    constructor(name, age, salary, gender) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.gender = gender;
    }

    static qSort(arr, key, order, start, end) {
        // base case
        if (start > end) {
            return;
        }
        const pivot = arr[end][key];
        let left = start - 1;
        for (let i = start; i <= end - 1; i++) {
            if (order === 'asc') {
                if (arr[i][key] < pivot) {
                    left++;
                    [arr[left], arr[i]] = [arr[i], arr[left]]
                }
            } else {
                if (arr[i][key] > pivot) {
                    left++;
                    [arr[left], arr[i]] = [arr[i], arr[left]]
                }
            }
        }
        [arr[left + 1], arr[end]] = [arr[end], arr[left + 1]]
        this.qSort(arr, key, order, start, left);
        this.qSort(arr, key, order, left + 2, end);
    }

    static sort(arr, key, order = 'asc') {
        const temp = [...arr];
        this.qSort(temp, key, order, 0, temp.length - 1);
        return temp;
    }
}

const person1 = new Person("Professor", 45, 115, "M");
const person2 = new Person("Tokyo", 29, 100, "F");
const person3 = new Person("Berlin", 33, 80, "M");
const person4 = new Person("Lisbon", 40, 118, "F");
const person5 = new Person("Rio", 26, 95, "M");

const persons = [person1, person2, person3, person4, person5];

const sortedArr = Person.sort(persons, "age")

const showTable = (sortedArr) => {
    const tbody = document.getElementById("tbody");
    tbody.innerHTML = "";
    for (const [_, value] of Object.entries(sortedArr)) {
        tr =
            "<tr>" +
            "<td>" +
            value.name +
            "</td>" +
            "<td>" +
            value.age.toString() +
            "</td>" +
            "<td>$" +
            value.salary.toString() +
            "</td>" +
            "<td>" +
            value.gender.toString() +
            "</td>" +
            "</tr>";
        tbody.innerHTML += tr;
    }
}
showTable(sortedArr);
